FROM registry.access.redhat.com/ubi8-minimal

RUN microdnf install python3 python3-pip

ADD LICENSE manage.py requirements.txt /app/src/
ADD trails /app/src/trails/
ADD wire_hobo /app/src/wire_hobo/

RUN pip3 install -r /app/src/requirements.txt && \
    rm /app/src/requirements.txt

ENV DJANGO_SETTINGS_MODULE=config.settings \
    PYTHONPATH=/app \
    ADDRPORT=0.0.0.0:8080

EXPOSE 8080

VOLUME /app/config \
       /app/data

WORKDIR /app/src

CMD python3 manage.py runserver \
            --settings="$DJANGO_SETTINGS_MODULE" \
            --pythonpath="$PYTHONPATH" \
            "$ADDRPORT"
